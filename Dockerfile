FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

RUN ls

RUN npm install

COPY . .

EXPOSE 3339

CMD ["npm", "start"]