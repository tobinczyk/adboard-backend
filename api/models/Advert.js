const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdvertSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  image:{
    type: String,
    required: true
  }
});

module.exports = Advert = mongoose.model('advert', AdvertSchema);
