const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '/images/');
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024
  },
  fileFilter: fileFilter
});

const Advert = require("../models/Advert");

router.get("/", (req, res, next) => {
  Advert.find()
  .then(adverts => {
    const response = {
      count: adverts.length,
      adverts: adverts.map(advert => {
        return {
          name: advert.name,
          price: advert.price,
          image: advert.image,
          _id: advert._id,
          request: {
            type: "GET",
            url: `http://${req.headers.host}/adverts/${advert._id}`
          }
        };
      })
    };
    if (adverts.length >= 0) {
      res.status(200).json(response);
    } else {
      res.status(404).json({
        message: 'No entries found'
      });
    }
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.post("/", upload.single('image'), (req, res, next) => {
  const product = new Advert({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    price: req.body.price,
    image: req.file.path
  });
  product
  .save()
  .then(result => {
    res.status(201).json({
      message: "Created advert successfully",
      createdAdvert: {
        name: result.name,
        price: result.price,
        _id: result._id,
        image: result.image,
        request: {
          type: 'GET',
          url: `http://${req.headers.host}/adverts/${result._id}`
        }
      }
    });
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.get("/:advertId", (req, res, next) => {
  const id = req.params.advertId;
  Advert.findById(id)
  .then(doc => {
    if (doc) {
      res.status(200).json({
        product: doc,
        request: {
          type: 'GET',
          url: 'http://localhost:3000/adverts'
        }
      });
    } else {
      res
      .status(404)
      .json({message: "No valid entry found for provided ID"});
    }
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({error: err});
  });
});

// router.patch("/:productId", (req, res, next) => {
//   const id = req.params.productId;
//   const updateOps = {};
//   for (const ops of req.body) {
//     updateOps[ops.propName] = ops.value;
//   }
//   Product.update({ _id: id }, { $set: updateOps })
//   .exec()
//   .then(result => {
//     res.status(200).json({
//       message: 'Product updated',
//       request: {
//         type: 'GET',
//         url: 'http://localhost:3000/products/' + id
//       }
//     });
//   })
//   .catch(err => {
//     console.log(err);
//     res.status(500).json({
//       error: err
//     });
//   });
// });
//
// router.delete("/:productId", (req, res, next) => {
//   const id = req.params.productId;
//   Product.remove({ _id: id })
//   .exec()
//   .then(result => {
//     res.status(200).json({
//       message: 'Product deleted',
//       request: {
//         type: 'POST',
//         url: 'http://localhost:3000/products',
//         body: { name: 'String', price: 'Number' }
//       }
//     });
//   })
//   .catch(err => {
//     console.log(err);
//     res.status(500).json({
//       error: err
//     });
//   });
// });

module.exports = router;