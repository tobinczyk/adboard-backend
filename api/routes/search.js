const express = require("express");
const router = express.Router();

const Advert = require("../models/Advert");

router.get("/", (req, res, next) => {
  if(req.query.tags)
    req.query.tags = JSON.parse(req.query.tags);

  const query = ParseToMongoScheme(req.query);
  Advert
  .find(query)
  .then(data => res.send(data));
});

function ParseToMongoScheme(query){
  let parsed = {...query};
  for(let [key, val] of Object.entries(parsed)){
    if(Advert.schema.paths[key]['instance'] === 'Number')
      Object.keys(val).forEach(el => val[el] = parseFloat(val[el]));
  }
  return parsed;
}

module.exports = router;